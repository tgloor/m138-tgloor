﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Google.Authenticator;

namespace TwoFactorAuth.Controllers
{
    public class HomeController : Controller
    {
        // key to generate the 2FA code
        private const string _key = "m183qrcodekeyabcdefghijklmnop";

        public ActionResult Index(string errorMsg = "")
        {
            ViewBag.ErrorMessage = errorMsg; // if there was an error, display it 
            return View();
        }

        public ActionResult Profile()
        {
            // Check if user is authenticated. If not, redirect to the login page
            if (Session["validLogin"] == null || !(bool)Session["validLogin"])
            {
                return RedirectToAction("Index", "Home", new { @errorMsg = "Nicht authentifiziert" });
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login2FA()
        {
            // Get user input and check data
            var username = Request["username"];
            var password = Request["password"];
            if (username == "admin" && password == "123")
            {
                // If the login is correct, create the 2FactorAuthenticator and generate the QR-Code or manual code
                var authenticator = new TwoFactorAuthenticator();
                var authResult = authenticator.GenerateSetupCode("M183 2FactorAuth", "2FA", _key, 300, 300);
                ViewBag.BarcodeImageUrl = authResult.QrCodeSetupImageUrl;
                ViewBag.SetupCode = authResult.ManualEntryKey;
                return View();
            }
            else
            {
                // Wrong login -> back to the login form
                return RedirectToAction("Index", "Home", new { @errorMsg = "Falsche Login Daten" });
            }     
        }

        public ActionResult Verify2Fa()
        {
            // Check if the token is correct
            var token = Request["token"];
            var authenticator = new TwoFactorAuthenticator();
            var isValid = authenticator.ValidateTwoFactorPIN(_key, token);
            if (isValid)
            {
                // If the token is correct, grant access to the profile page
                Session["validLogin"] = true;
                return RedirectToAction("Profile");
            }
            else
            {
                // Wrong token -> back to the login page
                return RedirectToAction("Index", "Home", new { @errorMsg = "QR-Code-Token ist falsch. Erneut versuchen." });
            }
            
        }

    }
}