**Informationsquellen**


***heise Security - https://www.heise.de/security***

*Quelle*

Heise Security ist ein Portal, auf dem laufend aktuelle Artikel zum Thema der Sicherheit veröffentlicht werden. Die Artikel sind teilweise so geschrieben, dass man ein gewisses Fachwissen im Bereich der Informatik benötigt. Es ist allerdings nicht all zu tragisch, wenn man sich nicht sehr gut darin auskennt.
Als normaler Nutzer der Website bleibt man ständig auf dem laufenden und bekommt immer alles mit, was gerade im Bereich der Sicherheit auf der Welt passiert. Da die Berichte auch so geschrieben sind, dass ein gewisses Know-How optimal ist, kann man als Applikationsentwickler umso mehr Informationen aus den Arktiel ziehen.

*Bericht: Lücke in älteren WhatsApp-Versionen erlaubte Codeausführung aus der Ferne - https://www.heise.de/security/meldung/Luecke-in-aelteren-WhatsApp-Versionen-erlaubte-Codeausfuehrung-aus-der-Ferne-4587119.html*

In dem Artikel geht es um eine Sicherheitslücke beim Messagerdienst WhatsApp von Facebook. Laut Facebook soll es mit dieser Lücke, welche nur auf alten Versionen funktioniert, möglich sein Code aus der Ferne auszuführen oder sogar ein DOS-Zustand auf dem Geräte zu erreichen.

Mich persönlich betrifft dieses Thema, wie auch die meisten anderen Personen mit einem mobilen Gerät, da die App sehr verbreitet ist und ich selber WhatsApp verwende. Da in dem Artikel genaue Angaben zu den betroffenen Versionen stehen, konnte ich überprüfen ob ich selber auch gefähredet bin oder ob ich schon eine neue Version habe.



***Melde- und Analysestelle Informationssicherung MELANI (z.B. Halbjahresberichte, aktuelle Gefahren) - https://www.melani.admin.ch/melani/de/home.html***


*Quelle*

MELANI ist eine Melde- und Analysestelle für Informationssicherung in Bezug auf Computersicherheit. Die Seite arbeitet mit mehreren Partnern zusammen, damit man immer auf dem neusten Stand bleiben kann. Als privater Nutzer ist die Seite sehr hilfreich, da man ständig einen guten Überblick hat, was im Bereich der Computersicherheit derzeit passiert. Wenn man eine Gefahr entdeckt, welche einen selber betrifft kann man dann direkt handeln, da es auch möglich ist einen Newsletter zu abonnieren. Da die Seite vom Bund ist kann man sich auch sicher sein, dass die Infos stimmen. Als Applikationsentwickler ist die Seite optimal selber Sicherheitslücken zu melden, da man wenn man in dem Bereich tätig ist oftmals auf diese Lücken zustösst. Dadurch kann man andere Personen oder Unternehmen warnen.

*Bericht: Social Engineering - https://www.melani.admin.ch/melani/de/home/themen/socialengineering.html*

In dem Bericht wird die Attacke "Social Engineering" sehr gut beschrieben. Es gibt eine allgemeine Erklärung, es werden die Auswirkungen und Gefahren klar dargestellt, Punkte zur Massnahme dagege werden aufgelistet und zum Schluss gibt es sogar noch ein Beispiel davon.

Dieser Bericht ist sehr gut geschrieben, so dass auch Personen diese Gefahr verstehen, welche nicht im Bereich der Informatik tätig sind. Dies ist auch wichtig, da diese Attacke sehr weit verbreitet ist und einen enormen Schaden anrichten kann. Dies betrifft eigentlich jede Person, welche Zugang zu heiklen Daten hat und sollte damit vertraut sein. Auch bei mir in der Firma ist "Social Engineering" ein grosses Thema und werden darauf auch speziell geschult.

***Spiegel - https://www.spiegel.de/netzwelt/***

*Quelle*

Der Spiegel ist eine weit verbreitete Zeitschrift in Deutschland. Im Abschnitt der Netzwelt, dreht sich alles über Technik. Nicht nur über Sicherheitslücken wird berichtet, sondern es werden auch neue Features getestet. Da dies eine solch grosse Zeitschrift ist, sind die Berichte auch entsprechend für die "breite Menge" geschrieben.
Als normaler Nutzer der Website bleibt man ständig auf dem neusten Stande was in der Welt der Technik passiert. Die Berichte sind verständlich geschrieben, so dass man auch ohne grosses Vorwissen alles verstehen kann. Als Applikationsentwickler hat man keinen grossen besonderen Nutzen gegenüber von normalen Usern. Allerdings, da man sowieso im Bereich der Technik arbeitet, ist es sehr angenehm auch darüber informiert zu werden, was generell passiert und nicht nur im Bereich auf die Informationssicherheit.

*Bericht: So sollen sich Bundeswehrmitarbeiter im Netz verhalten - https://www.spiegel.de/netzwelt/netzpolitik/bundeswehr-leitfaden-fuer-verhalten-in-sozialen-netzwerken-vorgestellt-a-1297265.html*

Der Bericht handelt von den Vorschriften, welche die Bundeswehr ihren Mitarbeitern vorgibt, was sie in Sozialen Medien teilen dürfen und was nicht. Es wird klar gemacht, dass durch einfache Posts im Internet viele Informationen an den Feind gelangen könnten. Damit dies nicht passiert, haben sie einige Massnahmen ergriffen. So sollte man sich bevor man etwas postet zum Beispiel fragen "Was, wenn das auf einer Titelseite landet?".
Dieses Thema ist nicht nur für die deutsche Bundeswehr wichtig, sondern auch bei jeder anderer Armee. Dadurch betrifft mich der Artikel auch, da ich bald ebenfalls den Militärdienst antreten muss. Für eine Person, welche kein Militärdienst leitet ist dieser Artikel allerdings auch spannend, da dies auch auf ein normales Unternehmen bezogen werden kann. Wenn sich eine Person kritisch im Internet äussert, kann dies unter Umständen auch Schaden für das Image des Unternehmens haben.