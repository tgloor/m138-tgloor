 Timo Gloor


**01_Keylogger**

Alle Tastatureingaben des Benutzers werden abgefangen und anschliessend laufend an einen Server weitergegeben.

Erweiterung: Gross- und Kleinbuchstaben & Umlaute werden korrekt erkannt



**02_Clickjacking**

Die Anmeldeseite des GIBZ wird mittels `<iframe>` eingebunden. Darüber wird allerdings ein zusätzliches Formular gelegt, so dass der Benutzer seine Daten im falschen Formular eingibt. Mit dem Sendebutton, werden seine Daten dann an einen Server weitergegeben.

Erweiterung: Nach Absenden der Daten, wird der Benutzer auf eine Fehlerseite des GIBZ weitergeleitet.


**03_TwoFactorAuthentication**

Ein Login soll mittels 2 Factor Authentication durchgeführt werden. Dazu soll der Benutzer zu erst sein Benutzername und Passwort eingeben, danach muss er mittels der Google Authenticator App einen QR-Code scannen und den erhaltenen Code eingeben.

Anmeldedaten:
*  Benutzername: admin
*  Passwort: 123

Erweiterung: Es werden zuerst die Login-Daten überprüft, erst wenn diese übereinstimmen wird der QR-Code generiert und angezeigt.


**06_Password**

Abgabe zusammen mit Alex Schmid. Gesamter Code & Beschreibung sind auf seinem Repository zu finden.


**07_HTTP_Digest_Authentication**

Für den Zugriff auf einen Server, soll zunächst eine HTTP Digest Authentication Methode angewandt werden, um den Zugriff zu verifizieren. Der Server schickt in seiner Response verschiedene Werte mit, aus welchen der Client dann selber bestimmte Token generieren muss. Stimmen diese generierte Token des Client mit den Erwartungen des Servers überein, ist der Zugriff gewährt. 

Die Tokens, welche der Client erstellen muss sind unteranderem eine Mischung aus Benutzername, Passwort und Server-Realm. Das ganze wird dann mit md5 verschlüsselt. Mit dem erhaltenen Wert kann man dann zusammen mit einem weiteren Wert die Response erstellen.

Anmeldedaten:

*  britney_s : hitMeBaby
*  bon_jovi : badMedicine
*  shakira : hipsDontLie
*  tom_j : sexbomb  



**09_CSRF**

Es wurde eine Datei erstellt, mit der verschieden CSRF-Attacken möglich sind. Dabei sollen verschiedene Daten eines erstellten Benutzers ohne sein Wissen geändert werden. Wie zum Beispiel das Passwort oder die E-Mail-Adresse.

Bei solch einer Attacke geht es darum, dass durch eine angeblich belanglose Aktivität der Schadcode ausgelöst wird.


**Informationsquellen**

Zusammenstellung einiger relevanten Informationsquellen zum Thema Applikationssicherheit. Dabei werden die einzelnen Quellen analysiert, kurz zusammengefasst und ein Beispiel Artikel präsentiert.